#variable declaraton
value1 = 10
value2 = 15
#Print command
puts "Hello World"
#Add
puts 2+2
puts value1 * value2
puts "My vale1 is #{value1}, value2 is #{value2}"
#using type casting
puts "First vale is #{value1}, second value is " + value2.to_s
#array
arr = ["My", "name", "is", "Chima"]
puts arr

arr1 = ["This", "is", "wonderful"]
#Looping over array
arr1.each_with_index do |value, index|
  puts "#{index}: " + value
end

#Methods
def say_my_name name, age
   puts "Your name is #{name} and you are #{age}!"
end

say_my_name "Enoch", 37

#Class
class Person
 def initialize name
   @name = name
 end

 def details
   puts "This person is called #{@name}!"
 end
end


person1 = Person.new("Chima")
person1.details

person2 = Person.new("Nkoli")
person2.details

#Collecting user input
class Person
 def initialize name, age
   @name = name
   @age = age
 end

 def details
   puts "This person is called #{@name}, and their age is #{@age}!"
 end
end

puts "What is your name?"
name = gets.chomp

puts "What is your age?"
age = gets.chomp

person2 = Person.new(name, age)
person2.details
